#include "audio.h"

#include "raylib.h"

namespace sokoban
{
	namespace config
	{
		namespace audio
		{
			float masterVolume = 1;
			float musicVolume = 1;
			float soundVolume = 1;

			namespace sounds
			{
				Sound selectOption;

				void InitMenu()
				{
					selectOption = LoadSound("res/assets/SFX/select_sound.mp3");
				}

				void InitGameloop()
				{

				}

				void Update()
				{
					SetSoundVolume(selectOption, soundVolume);
				}

				void DeInitMenu()
				{
					UnloadSound(selectOption);
				}

				void DeInitGameloop()
				{

				}
			}

			namespace music
			{
				Music menu;
				Music gameloop;

				void InitMenu()
				{
					if (!IsMusicPlaying(menu))
					{
						menu = LoadMusicStream("res/assets/Music/menu_ost.mp3");
						PlayMusicStream(menu);
					}
				}

				void InitGameloop()
				{
					if (!IsMusicPlaying(gameloop))
					{
						gameloop = LoadMusicStream("res/assets/Music/gameloop_ost.mp3");
						PlayMusicStream(gameloop);
					}
				}

				void Update()
				{
					SetMusicVolume(menu, musicVolume);
				}

				void DeInitMenu()
				{
					StopMusicStream(menu);
					UnloadMusicStream(menu);
				}

				void DeInitGameloop()
				{
					StopMusicStream(gameloop);
					UnloadMusicStream(gameloop);
				}
			}

			void InitMenu()
			{
				sounds::InitMenu();
				music::InitMenu();
			}

			void InitGameloop()
			{
				sounds::InitGameloop();
				music::InitGameloop();
			}

			void Init()
			{
				InitAudioDevice(); //init audio device
				SetMasterVolume(0.5f); //set initial volume	

				InitMenu();
			}

			void DeInitMenu()
			{
				sounds::DeInitMenu();
				music::DeInitMenu();
			}

			void DeInitGameloop()
			{
				sounds::DeInitGameloop();
				music::DeInitGameloop();
			}
		}
	}
}