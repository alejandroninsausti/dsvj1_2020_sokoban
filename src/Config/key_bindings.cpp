#include "key_bindings.h"

#include "raylib.h"

namespace sokoban
{
	namespace config
	{
		namespace keys
		{
			KeyBindings keys;

			void Init()
			{
				SetExitKey(0); //set raylib exit key to NULL

				keys.moveUp = KEY_UP;
				keys.moveDown = KEY_DOWN;
				keys.moveLeft = KEY_LEFT;
				keys.moveRight = KEY_RIGHT;
				keys.quit = KEY_ESCAPE;
			}
		}
	}
}