#ifndef KEY_BINDINGS_H
#define KEY_BINDINGS_H

namespace sokoban
{
	namespace config
	{
		namespace keys
		{
			struct KeyBindings
			{
				short moveUp;
				short moveDown;
				short moveLeft;
				short moveRight;
				short quit;
			};

			void Init();

			extern KeyBindings keys;
		}		
	}
}

#endif // !KEY_BINDINGS_H
