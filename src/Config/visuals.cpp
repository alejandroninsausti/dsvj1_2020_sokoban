#include "visuals.h"

#include "Utility/utility_menu.h"

namespace sokoban
{
	namespace config
	{
		namespace visuals
		{
			RESOLUTIONS res;
			int screenWidth;
			int screenHeight;

			Texture2D background;
			Texture2D whiteFilter;

			namespace menu
			{
				//----------------fonts-----------------
				Font menuFont;
				Font linkFont;


				void Init()
				{
					menuFont = GetFontDefault();
					linkFont = menuFont;
				}

				void DeInit()
				{
					UnloadFont(menuFont);
					UnloadFont(linkFont);
				}
			}

			namespace gameloop
			{
				//----------------fonts-----------------
				Font gameloopFont;
				//--------------textures----------------
				Texture2D bed;
				Texture2D kid;
				Texture2D kidInBed;
				Texture2D ghost;
				Texture2D wall;

				void Init()
				{
					gameloopFont = menu::menuFont;

					bed = LoadTexture("res/assets/Images/empty_bed.png");
					kid = LoadTexture("res/assets/Images/kid.png");
					kidInBed = LoadTexture("res/assets/Images/kid_bed.png");
					ghost = LoadTexture("res/assets/Images/ghost.png");
					wall = LoadTexture("res/assets/Images/wall.png");
				}

				void DeInit()
				{
					UnloadFont(gameloopFont);

					UnloadTexture(bed);
					UnloadTexture(kid);
					UnloadTexture(kidInBed);
					UnloadTexture(ghost);
					UnloadTexture(wall);
				}
			}

			void InitScreenSize(RESOLUTIONS newRes)
			{
				res = newRes;

				switch (res)
				{
				case RESOLUTIONS::LOW_RES:
					screenWidth = 720;
					screenHeight = 480;
					break;
				case RESOLUTIONS::MID_RES:
					screenWidth = 800;
					screenHeight = 600;
					break;
				case RESOLUTIONS::HIGH_RES:
					screenWidth = 1152;
					screenHeight = 900;
					break;
				default:
					break;
				}
								
				SetWindowSize(screenWidth, screenHeight);
			}

			void Init()
			{
				InitScreenSize(RESOLUTIONS::MID_RES);

				InitWindow(screenWidth, screenHeight, TextFormat("Basic Sokoban %s", utility::menu::gameVersion)); //open OpenGL window
				SetTargetFPS(60); //set max frame rate
								
				background = LoadTexture("res/assets/Images/background.png");
				background.height = screenHeight;
				background.width = screenWidth;
				whiteFilter = LoadTexture("res/assets/Images/white_filter.png");

				menu::Init();
			}

			void UpdateBackgroundSize()
			{
				background.height = screenHeight;
				background.width = screenWidth;
			}
		}
	}
}