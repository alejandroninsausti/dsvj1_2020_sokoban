#ifndef VISUALS_H
#define VISUALS_H

#include "raylib.h"

namespace sokoban
{
	namespace config
	{
		namespace visuals
		{
			enum class RESOLUTIONS { LOW_RES, MID_RES, HIGH_RES };

			extern RESOLUTIONS res;
			extern int screenWidth;
			extern int screenHeight;

			extern Texture2D background;
			extern Texture2D whiteFilter;

			namespace menu
			{
				//----------------fonts-----------------
				extern Font menuFont;
				extern Font linkFont;

				void Init();
				void DeInit();
			}

			namespace gameloop
			{
				//----------------fonts-----------------
				extern Font gameloopFont;
				//--------------textures----------------
				extern Texture2D bed;
				extern Texture2D kid;
				extern Texture2D kidInBed;
				extern Texture2D ghost;
				extern Texture2D wall;

				void Init();
				void DeInit();
			}

			void InitScreenSize(RESOLUTIONS newRes);
			void Init();
			void UpdateBackgroundSize();
		}
	}
}

#endif // !VISUALS_H
