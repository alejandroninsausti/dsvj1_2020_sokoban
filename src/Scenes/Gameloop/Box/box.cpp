#include "box.h"

#include "Scenes/Gameloop/Map/map.h"

namespace sokoban
{
	namespace gameloop
	{
		namespace box
		{
			short positionedBoxes;
			short totalBoxes;

			void Init()
			{
				using namespace map;

				short counter = 0;

				for (short i = 0; i < mapMaxHeight; i++)
				{
					for (short j = 0; j < mapMaxWidth; j++)
					{
						if (tile[i][j].type == TYPES::CONTAINER_EMPTY)
						{
							counter++;
						}
					}
				}

				positionedBoxes = 0;
				totalBoxes = counter;
			}

			void CheckBoxContainers()
			{
				using namespace map;

				short counter = 0;

				for (short i = 0; i < mapMaxHeight; i++)
				{
					for (short j = 0; j < mapMaxWidth; j++)
					{
						if (tile[i][j].type == TYPES::CONTAINER_EMPTY && tile[i][j].objectAbove == OBJECT::BOX)
						{
							tile[i][j].type = TYPES::CONTAINER_FULL;
						}	

						if (tile[i][j].type == TYPES::CONTAINER_FULL)
						{
							if (tile[i][j].objectAbove == OBJECT::BOX)
							{
								counter++;
							}
							else
							{
								tile[i][j].type = TYPES::CONTAINER_EMPTY;
							}
						}
					}
				}

				positionedBoxes = counter;
			}
		}
	}
}