#ifndef BOX_H
#define BOX_H

#include "raylib.h"

namespace sokoban
{
	namespace gameloop
	{
		namespace box
		{
			extern short positionedBoxes;
			extern short totalBoxes;

			void Init();
			void CheckBoxContainers();
		}
	}
}

#endif // !BOX_H