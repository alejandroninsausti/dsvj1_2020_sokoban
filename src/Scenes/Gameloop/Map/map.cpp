#include "map.h"

#include "Config/visuals.h"
#include "Utility/utility_calc.h"

using namespace sokoban::utility::calc;

namespace sokoban
{
	namespace gameloop
	{
		namespace map
		{
			Tile tile[mapMaxHeight][mapMaxWidth];
			float textureScale;
			float ghostScale;

			static void SetPosition(short i, short j)
			{
				using namespace config::visuals;
				using namespace utility::calc;

				float xMiddle = config::visuals::screenWidth * 0.5f;
				float yMiddle = config::visuals::screenHeight * 0.5f;
				tile[i][j].rec.width = config::visuals::screenWidth / (NumToFloat(mapMaxWidth) * 1.5f);
				tile[i][j].rec.height = config::visuals::screenHeight / (NumToFloat(mapMaxHeight) * 1.25f);
				tile[i][j].rec.x = xMiddle + tile[i][j].rec.width * (j - mapMaxWidth / 2);
				tile[i][j].rec.y = yMiddle + tile[i][j].rec.height * (i - mapMaxHeight / 2);
			}

			static void SetTexturePosition(short i, short j)
			{
				tile[i][j].texturePosition.x = tile[i][j].rec.x;
				tile[i][j].texturePosition.y = tile[i][j].rec.y;
			}

			static void InitTextures()
			{
				using namespace config::visuals::gameloop;

				bed.width = tile[0][0].rec.width;
				kid.width = tile[0][0].rec.width;
				kidInBed.width = tile[0][0].rec.width;
				ghost.width = tile[0][0].rec.width;
				wall.width = tile[0][0].rec.width;

				bed.height = tile[0][0].rec.height;
				kid.height = tile[0][0].rec.height;
				kidInBed.height = tile[0][0].rec.height;
				ghost.height = tile[0][0].rec.height;
				wall.height = tile[0][0].rec.height;
			}

			void Init()
			{
				for (short i = 0; i < mapMaxHeight; i++)
				{
					for (short j = 0; j < mapMaxWidth; j++)
					{						
						SetPosition(i, j);
						SetTexturePosition(i, j);
					}
				}

				InitTextures();
				textureScale = 0.003f * config::visuals::screenWidth;
				ghostScale = 0.0003f * config::visuals::screenWidth;
			}

			void UpdateTileObject(MatrixPos pos, MatrixPos newPos)
			{
				using namespace map;
				
				if (tile[newPos.i][newPos.j].objectAbove == OBJECT::BOX)
				{
					short iMod = newPos.i - pos.i;
					short jMod = newPos.j - pos.j;

					if (tile[newPos.i + iMod][newPos.j + jMod].objectAbove == OBJECT::NONE)
					{
						tile[newPos.i + iMod][newPos.j + jMod].objectAbove = tile[newPos.i][newPos.j].objectAbove;
						tile[newPos.i][newPos.j].objectAbove = tile[pos.i][pos.j].objectAbove;
						tile[pos.i][pos.j].objectAbove = OBJECT::NONE;
					}
				}
				else
				{
					tile[newPos.i][newPos.j].objectAbove = tile[pos.i][pos.j].objectAbove;
					tile[pos.i][pos.j].objectAbove = OBJECT::NONE;
					//tile[newPos.i][newPos.j].texturePosition = tile[pos.i][pos.j].texturePosition;
				}
			}

			void Draw()
			{
				using namespace config::visuals::gameloop;

				for (short i = 0; i < mapMaxHeight; i++)
				{
					for (short j = 0; j < mapMaxWidth; j++)
					{
#if _DEBUG
						switch (tile[i][j].type)
						{
						case TYPES::WALL:
							DrawRectangleRec(tile[i][j].rec, BLACK);
							break;
						case TYPES::FLOOR:
							DrawRectangleRec(tile[i][j].rec, DARKBLUE);
							break;						
						case TYPES::CONTAINER_EMPTY:
							DrawRectangleRec(tile[i][j].rec, RED);
							break;
						case TYPES::CONTAINER_FULL:
							DrawRectangleRec(tile[i][j].rec, GREEN);
							break;
						default:
							break;
						}

						switch (tile[i][j].objectAbove)
						{
						case OBJECT::BOX:
							DrawRectangleRec(tile[i][j].rec, BROWN);
							break;
						case OBJECT::CHAR:
							DrawRectangleRec(tile[i][j].rec, YELLOW);
							break;
						default:
							break;
						}
#endif // _DEBUG
						switch (tile[i][j].type)
						{
						case TYPES::WALL:
							DrawRectangleRec(tile[i][j].rec, MAROON);
							DrawTextureEx(wall, tile[i][j].texturePosition, 0, textureScale, WHITE);
							break;
						case TYPES::FLOOR:
							DrawRectangleRec(tile[i][j].rec, DARKBLUE);
							switch (tile[i][j].objectAbove)
							{
							case OBJECT::BOX:
								DrawTextureEx(bed, tile[i][j].texturePosition, 0, textureScale, WHITE);
								break;
							case OBJECT::CHAR:
								DrawTextureEx(ghost, tile[i][j].texturePosition, 0, ghostScale, WHITE);
								break;
							default:
								break;
							}
							break;
						case TYPES::CONTAINER_EMPTY:
							DrawRectangleRec(tile[i][j].rec, DARKBLUE);
							DrawTextureEx(kid, tile[i][j].texturePosition, 0, textureScale, WHITE);
							break;
						case TYPES::CONTAINER_FULL:
							DrawRectangleRec(tile[i][j].rec, DARKBLUE);
							DrawTextureEx(kidInBed, tile[i][j].texturePosition, 0, textureScale, WHITE);
							break;
						default:
							break;
						}						
					}
				}
				
			}

			void DeInit()
			{
				for (short i = 0; i < mapMaxHeight; i++)
				{
					for (short j = 0; j < mapMaxWidth; j++)
					{
						tile[i][j].type = TYPES::EMPTY;
						tile[i][j].objectAbove = OBJECT::NONE;
					}
				}
			}
		}
	}
}