#ifndef MAP_H
#define MAP_H

#include "raylib.h"

#include "Utility/utility_calc.h"

namespace sokoban
{
	namespace gameloop
	{
		namespace map
		{
			const short mapMaxHeight = 16;
			const short mapMaxWidth = 16;

			enum class TYPES { EMPTY, WALL, FLOOR, BOX, CONTAINER_EMPTY, CONTAINER_FULL };
			enum class OBJECT { NONE, BOX, CHAR };
			struct Tile
			{
				Rectangle rec;
				Vector2 texturePosition;
				TYPES type;
				OBJECT objectAbove = OBJECT::NONE;
			};

			extern Tile tile[mapMaxHeight][mapMaxWidth];

			void Init();
			void UpdateTileObject(utility::calc::MatrixPos pos, utility::calc::MatrixPos newPos);
			void Draw();
			void DeInit();
		}
	}
}

#endif // !MAP_H
