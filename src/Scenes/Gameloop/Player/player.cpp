#include "player.h"

#include "Config/key_bindings.h"
#include "Scenes/Gameloop/Map/map.h"

using namespace sokoban::utility::calc;

namespace sokoban
{
	namespace gameloop
	{
		namespace player
		{
			MatrixPos matrixPos;

			static MOVEMENT GetInput()
			{
				using namespace config::keys;

				if (IsKeyPressed(keys.moveUp))
				{
					return MOVEMENT::UP;
				}
				else if (IsKeyPressed(keys.moveDown))
				{
					return MOVEMENT::DOWN;
				}
				else if (IsKeyPressed(keys.moveLeft))
				{
					return MOVEMENT::LEFT;
				}
				else if (IsKeyPressed(keys.moveRight))
				{
					return MOVEMENT::RIGHT;
				}
				else
				{
					return MOVEMENT::NONE;
				}
			}

			static bool ViableTile(MOVEMENT move)//check if the new tile ia available
			{
				using namespace map;

				short iMod = 0;
				short jMod = 0;

				switch (move)
				{
				case MOVEMENT::UP:
					iMod--;
					break;
				case MOVEMENT::DOWN:
					iMod++;
					break;
				case MOVEMENT::LEFT:
					jMod--;
					break;
				case MOVEMENT::RIGHT:
					jMod++;
					break;
				default:
					break;
				}

				short i = matrixPos.i + iMod;
				short j = matrixPos.j + jMod;

				//if the new tile has a box, check that the next one is also available
				if (tile[i][j].objectAbove == OBJECT::BOX)
				{
					return
						(IntIsWithinRange(i + iMod, 0, mapMaxHeight) &&
							IntIsWithinRange(j + jMod, 0, mapMaxWidth))
						&&
						(tile[i + iMod][j + jMod].type == TYPES::CONTAINER_EMPTY ||
							tile[i + iMod][j + jMod].type == TYPES::FLOOR)
						&& 
						tile[i + iMod][j + jMod].objectAbove == OBJECT::NONE;
				}
				else
				{
					switch (tile[i][j].type)
					{
					case TYPES::FLOOR:
					case TYPES::CONTAINER_EMPTY:
						return true;
						break;
					default:
						return false;
						break;
					}
				}
			}

			static bool MovementIsPosible(MOVEMENT move) //return if move is on valid tile and inside matrix
			{
				using namespace map;
				using namespace utility::calc;

				switch (move)
				{
				case MOVEMENT::UP:
					return matrixPos.i - 1 >= 0 && ViableTile(move);
					break;
				case MOVEMENT::DOWN:
					return matrixPos.i + 1 < mapMaxHeight && ViableTile(move);
					break;
				case MOVEMENT::LEFT:
					return matrixPos.j - 1 >= 0 && ViableTile(move);
					break;
				case MOVEMENT::RIGHT:
					return matrixPos.j + 1 < mapMaxWidth && ViableTile(move);
					break;
				default:
					return false;
					break;
				}
			}

			void Update()
			{
				MOVEMENT move = GetInput();
				if (MovementIsPosible(move))
				{
					using namespace map;
					using namespace utility::calc;

					UpdateTileObject(matrixPos, MoveInMatrix(matrixPos, move));
					matrixPos = MoveInMatrix(matrixPos, move);
				}
			}
		}
	}
}