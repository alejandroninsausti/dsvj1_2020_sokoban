#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"

#include "Utility/utility_calc.h"

namespace sokoban
{
	namespace gameloop
	{
		namespace player
		{
			extern utility::calc::MatrixPos matrixPos;

			void Update();
		}
	}
}

#endif // !PLAYER_H
