#include "gameloop.h"

#include "raylib.h"

#include "Config/key_bindings.h"
#include "Config/visuals.h"
#include "Config/audio.h"
#include "Scenes/Menu/Settings/menu_settings.h"
#include "Scenes/Gameloop/Map/map.h"
#include "Scenes/Gameloop/Player/player.h"
#include "Scenes/Gameloop/Box/box.h"
#include "Utility/utility_calc.h"
#include "Utility/utility_menu.h"

namespace sokoban
{
	namespace gameloop
	{
		bool pause;
		bool quit;

		Rectangle pauseTextbox;
		Rectangle resumeTextbox;
		Rectangle optionsTextbox;
		Rectangle quitTextbox;

		Texture2D pauseFrame;

		float letterSize;
		float pauseSize;

		static void InitConfigVariables()
		{
			using namespace utility::calc;

			letterSize = NumToFloat(config::visuals::screenWidth) * 0.04f;
			pauseSize = letterSize * 3;
						
			pauseTextbox = utility::menu::InitTextbox(pauseSize, 6, 5, 2);
			resumeTextbox = utility::menu::InitTextbox(letterSize, 7, 5, 4.5f);
			optionsTextbox = utility::menu::InitTextbox(letterSize, 7, 5, 5.5f);
			quitTextbox = utility::menu::InitTextbox(letterSize, 17, 5, 6.5f);

			pauseFrame = config::visuals::whiteFilter;
			pauseFrame.height = NumToInt(quitTextbox.y + quitTextbox.height - pauseTextbox.y);
			pauseFrame.width = NumToInt(pauseTextbox.width * 1.55f);

			map::Init();

			config::visuals::gameloop::Init();
			config::audio::InitGameloop();

			PlayMusicStream(config::audio::music::gameloop);
		}

		static void Init()
		{
			InitConfigVariables();

			quit = false;
			pause = false;
		}

		static void DeInitConfigVariables()
		{
			config::visuals::gameloop::DeInit();
			config::audio::DeInitGameloop();
		}

		static void DeInit()
		{
			DeInitConfigVariables();
			map::DeInit();
		}

		static void Update()
		{
			UpdateMusicStream(config::audio::music::gameloop);
			if (pause)
			{
				using namespace utility::menu;

				quit = IsKeyReleased(config::keys::keys.quit);

				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					if (OptionClicked(resumeTextbox))
					{
						pause = false;
					}
					if (OptionClicked(optionsTextbox))
					{
						DeInitConfigVariables();
						sokoban::config::audio::InitMenu();
						sokoban::config::visuals::menu::Init();
						menu::menu_settings::SettingsMenu();
						sokoban::config::audio::DeInitMenu();
						sokoban::config::visuals::menu::DeInit();
						InitConfigVariables();
					}
					else if (OptionClicked(quitTextbox))
					{
						quit = true;
					}
				}
			}
			else
			{
				player::Update();
				box::CheckBoxContainers();

				pause = IsKeyReleased(config::keys::keys.quit) || box::positionedBoxes >= box::totalBoxes;
			}

		}

		static void Draw()
		{
			using namespace config::visuals::menu;
			using namespace config::visuals::gameloop;
			using namespace utility::menu;
			using namespace utility::calc;

			BeginDrawing();
			//draw Background
			DrawTexture(config::visuals::background, 0, 0, WHITE);
			map::Draw();
			DrawTextOnly(GetFontDefault(), "KEYS: ARROWS to move, ESC to pause", letterSize * 0.7f, WHITE, 5, 0.5f);


			if (pause)
			{
				DrawTexture(pauseFrame, NumToInt(quitTextbox.x * 0.6f), NumToInt(pauseTextbox.y), GRAY);
				box::positionedBoxes < box::totalBoxes ? DrawTextOnly(gameloopFont, "Pause", pauseSize, WHITE, 5, 2) :
					DrawTextOnly(gameloopFont, "Game Over", pauseSize, WHITE, 5, 2);
				DrawTextbox(menuFont, "Resume", resumeTextbox, letterSize, WHITE, DARKGRAY);
				DrawTextbox(menuFont, "Options", optionsTextbox, letterSize, WHITE, DARKGRAY);
				DrawTextbox(menuFont, "Go Back To Menu", quitTextbox, letterSize, WHITE, DARKGRAY);
			}
			ClearBackground(GRAY);
			EndDrawing();
		}		
	
		void Gameloop()
		{
			Init();

			do
			{
				Update();
				Draw();
			} while (!quit && !WindowShouldClose());

			DeInit();
		}	
	}
}