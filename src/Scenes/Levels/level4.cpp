#include "level4.h"

#include "Scenes/Gameloop/Player/player.h"
#include "Scenes/Gameloop/Map/map.h"
#include "Scenes/Gameloop/gameloop.h"
#include "Scenes/Gameloop/Box/box.h"
#include "Utility/utility_calc.h"

namespace sokoban
{
	namespace levels
	{
		static void Init()
		{
			using namespace gameloop::map;

			for (short i = 0; i < mapMaxHeight; i++)
			{
				for (short j = 0; j < mapMaxWidth; j++)
				{
					//set all tile types
					switch (j)
					{
					case mapMaxWidth / 2 - 2:
						switch (i)
						{
						case mapMaxHeight / 2 - 3:
						case mapMaxHeight / 2 + 3:
							tile[i][j].type = TYPES::WALL;
							break;
						case mapMaxHeight / 2 - 1:
						case mapMaxHeight / 2 + 1:
							tile[i][j].objectAbove = OBJECT::BOX;
							tile[i][j].type = TYPES::FLOOR;
							break;
						case mapMaxHeight / 2:
							tile[i][j].objectAbove = OBJECT::CHAR;
							gameloop::player::matrixPos.i = i;
							gameloop::player::matrixPos.j = j;
							tile[i][j].type = TYPES::FLOOR;
							break;
						default:
							if (utility::calc::IntIsWithinRange(i, mapMaxHeight / 2 - 3, mapMaxHeight / 2 + 3))
							{
								tile[i][j].type = TYPES::FLOOR;
							}
							else
							{
								tile[i][j].type = TYPES::EMPTY;
							}
							break;
						}
						break;
					case mapMaxWidth / 2 - 1:
						switch (i)
						{
						case mapMaxHeight / 2 - 3:
						case mapMaxHeight / 2 + 3:
							tile[i][j].type = TYPES::WALL;
							break;
						case mapMaxHeight / 2 - 2:
						case mapMaxHeight / 2 + 2:
							tile[i][j].type = TYPES::FLOOR;
							break;
						default:
							if (utility::calc::IntIsWithinRange(i, mapMaxHeight / 2 - 3, mapMaxHeight / 2 + 3))
							{
								tile[i][j].objectAbove = OBJECT::BOX;
								tile[i][j].type = TYPES::FLOOR;
							}
							else
							{
								tile[i][j].type = TYPES::EMPTY;
							}
							break;
						}
						break;
					case mapMaxWidth / 2:
						switch (i)
						{
						case mapMaxHeight / 2:
							tile[i][j].objectAbove = OBJECT::NONE;
							tile[i][j].type = TYPES::FLOOR;
							break;
						default:
							if (utility::calc::IntIsWithinRange(i, mapMaxHeight / 2 - 3, mapMaxHeight / 2 + 3))
							{
								tile[i][j].type = TYPES::WALL;
							}
							else
							{
								tile[i][j].type = TYPES::EMPTY;
							}
							break;
						}
						break;
					case mapMaxWidth / 2 + 1:
						switch (i)
						{
						case mapMaxHeight / 2 - 3:
						case mapMaxHeight / 2 + 3:
							tile[i][j].type = TYPES::WALL;
							break;
						case mapMaxHeight / 2 - 2:
						case mapMaxHeight / 2 + 2:
							tile[i][j].type = TYPES::CONTAINER_EMPTY;
							break;
						default:
							if (utility::calc::IntIsWithinRange(i, mapMaxHeight / 2 - 3, mapMaxHeight / 2 + 3))
							{
								tile[i][j].type = TYPES::FLOOR;
							}
							else
							{
								tile[i][j].type = TYPES::EMPTY;
							}
							break;
						}
						break;
					case mapMaxWidth / 2 + 2:
						switch (i)
						{
						case mapMaxHeight / 2 - 3:
						case mapMaxHeight / 2 + 3:
							tile[i][j].type = TYPES::WALL;
							break;
						case mapMaxHeight / 2 - 1:
						case mapMaxHeight / 2 + 1:
							tile[i][j].type = TYPES::FLOOR;
							break;
						default:
							if (utility::calc::IntIsWithinRange(i, mapMaxHeight / 2 - 3, mapMaxHeight / 2 + 3))
							{
								tile[i][j].type = TYPES::CONTAINER_EMPTY;
							}
							else
							{
								tile[i][j].type = TYPES::EMPTY;
							}
							break;
						}
						break;
					case mapMaxWidth / 2 - 3:
					case mapMaxWidth / 2 + 3:
						switch (i)
						{
						case mapMaxHeight / 2 - 3:
						case mapMaxHeight / 2 + 3:
							tile[i][j].type = TYPES::WALL;
							break;
						default:
							if (utility::calc::IntIsWithinRange(i, mapMaxHeight / 2 - 3, mapMaxHeight / 2 + 3))
							{
								tile[i][j].type = TYPES::FLOOR;
							}
							else
							{
								tile[i][j].type = TYPES::EMPTY;
							}
							break;
						}
						break;
					case mapMaxWidth / 2 - 4:
					case mapMaxWidth / 2 + 4:
						if (utility::calc::IntIsWithinRange(i, mapMaxHeight / 2 - 3, mapMaxHeight / 2 + 3))
						{
							tile[i][j].type = TYPES::WALL;
						}
						else
						{
							tile[i][j].type = TYPES::EMPTY;
						}
						break;
					default:
						tile[i][j].type = TYPES::EMPTY;
						break;
					}
				}
			}

			gameloop::box::Init();
		}

		void Level4()
		{
			Init();

			gameloop::Gameloop();
		}
	}
}