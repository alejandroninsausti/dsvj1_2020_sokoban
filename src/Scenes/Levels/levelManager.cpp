#include "levelManager.h"

#include "raylib.h"

#include "level_1.h"
#include "level2.h"
#include "level3.h"
#include "level4.h"
#include "level5.h"
#include "Utility/utility_menu.h"
#include "Utility/utility_calc.h"
#include "Config/key_bindings.h"
#include "Config/visuals.h"
#include "Config/audio.h"

namespace sokoban
{
	namespace levels
	{
		
		Rectangle textBoxLevel1;
		Rectangle textBoxLevel2;
		Rectangle textBoxLevel3;
		Rectangle textBoxLevel4;
		Rectangle textBoxLevel5;
		Rectangle textBoxExit;

		float letterSize;

		static void Init()
		{
			using namespace utility::menu;
			using namespace config::visuals;

			letterSize = utility::calc::NumToFloat(screenWidth) * 0.045f;

			textBoxLevel1 = InitTextbox(letterSize, 1, 3, 4);
			textBoxLevel2 = InitTextbox(letterSize, 1, 4, 4);
			textBoxLevel3 = InitTextbox(letterSize, 1, 5, 4);
			textBoxLevel4 = InitTextbox(letterSize, 1, 6, 4);
			textBoxLevel5 = InitTextbox(letterSize, 1, 7, 4);
			textBoxExit = InitTextbox(letterSize * 2, 3.5f, 5, 9);
		}

		static void Draw()
		{
			using namespace utility::menu;
			using namespace config::visuals;

			BeginDrawing();
			
			//draw background
			DrawTexture(background, 0, 0, WHITE);

			//draw levels
			DrawTextbox(menu::menuFont, "1", textBoxLevel1, letterSize, DARKGRAY, GRAY);
			DrawTextbox(menu::menuFont, "2", textBoxLevel2, letterSize, DARKGRAY, GRAY);
			DrawTextbox(menu::menuFont, "3", textBoxLevel3, letterSize, DARKGRAY, GRAY);
			DrawTextbox(menu::menuFont, "4", textBoxLevel4, letterSize, DARKGRAY, GRAY);
			DrawTextbox(menu::menuFont, "5", textBoxLevel5, letterSize, DARKGRAY, GRAY);

			//draw exit
			DrawTextbox(menu::menuFont, "Exit", textBoxExit, letterSize * 2, DARKGRAY, GRAY);

			ClearBackground(DARKGRAY);
			EndDrawing();
		}

		void LevelManager()
		{
			using namespace config::audio;

			Init();

			do
			{
				UpdateMusicStream(music::menu);

				//Input | Update
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					if (utility::menu::OptionClicked(textBoxLevel1))
					{
						PlaySound(sounds::selectOption);
						config::visuals::menu::DeInit();
						config::audio::DeInitMenu();
						Level1();
						config::audio::InitMenu();
						config::visuals::menu::Init();
						Init();
					}
					else if (utility::menu::OptionClicked(textBoxLevel2))
					{
						PlaySound(sounds::selectOption);
						config::visuals::menu::DeInit();
						config::audio::DeInitMenu();
						Level2();
						config::audio::InitMenu();
						config::visuals::menu::Init();
						Init();
					}
					else if (utility::menu::OptionClicked(textBoxLevel3))
					{
						PlaySound(sounds::selectOption);
						config::visuals::menu::DeInit();
						config::audio::DeInitMenu();
						Level3();
						config::audio::InitMenu();
						config::visuals::menu::Init();
						Init();
					}
					else if (utility::menu::OptionClicked(textBoxLevel4))
					{
						PlaySound(sounds::selectOption);
						config::visuals::menu::DeInit();
						config::audio::DeInitMenu();
						Level4();
						config::audio::InitMenu();
						config::visuals::menu::Init();
						Init();
					}
					else if (utility::menu::OptionClicked(textBoxLevel5))
					{
						PlaySound(sounds::selectOption);
						config::visuals::menu::DeInit();
						config::audio::DeInitMenu();
						Level5();
						config::audio::InitMenu();
						config::visuals::menu::Init();
						Init();
					}
				}

				//Draw
				Draw();
			} while (!IsKeyReleased(config::keys::keys.quit) && !WindowShouldClose() &&
				(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));
		}
	}
}
