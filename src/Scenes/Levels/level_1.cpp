#include "level_1.h"

#include "Scenes/Gameloop/Player/player.h"
#include "Scenes/Gameloop/Map/map.h"
#include "Scenes/Gameloop/gameloop.h"
#include "Scenes/Gameloop/Box/box.h"
#include "Utility/utility_calc.h"

namespace sokoban
{
	namespace levels
	{
		static void Init()
		{
			using namespace gameloop::map;

			for (short i = 0; i < mapMaxHeight; i++)
			{
				for (short j = 0; j < mapMaxWidth; j++)
				{
					//set all tile types
					switch (i)
					{
					case mapMaxHeight / 2 - 1:
					case mapMaxHeight / 2 + 1:
						tile[i][j].type = TYPES::WALL;
						break;
					case mapMaxHeight / 2:
						switch (j)
						{
						case 0:
						case mapMaxWidth - 1:
							tile[i][j].type = TYPES::WALL;
							break;
						case 1:
							tile[i][j].objectAbove = OBJECT::CHAR;
							gameloop::player::matrixPos.i = i;
							gameloop::player::matrixPos.j = j;
							tile[i][j].type = TYPES::FLOOR;
							break;
						case mapMaxWidth / 2:
							tile[i][j].objectAbove = OBJECT::BOX;
							tile[i][j].type = TYPES::FLOOR;
							break;
						case mapMaxWidth - 2:
							tile[i][j].type = TYPES::CONTAINER_EMPTY;
							break;
						default:
							tile[i][j].type = TYPES::FLOOR;
							break;
						}
						break;
					default:
						tile[i][j].type = TYPES::EMPTY;
						break;
					}					
				}
			}

			gameloop::box::Init();
		}
		
		void Level1()
		{
			Init();

			gameloop::Gameloop();
		}
	}
}