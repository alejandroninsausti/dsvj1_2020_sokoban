#include "menu_credits.h"

#include "raylib.h"

#include "Config/audio.h"
#include "Utility/utility_menu.h"
#include "Config/visuals.h"
#include "Config/key_bindings.h"

using namespace sokoban::config::audio;
using namespace sokoban::config::keys;
using namespace sokoban::config::visuals;
using namespace sokoban::utility;

namespace sokoban
{
	namespace menu
	{
		namespace menu_credits
		{
			float letterSize;
			float letterSpacing;
			float linkSize;
			float linkSpacing;

			Rectangle textBoxExit;

			void Init()
			{
				letterSize = screenWidth * 0.03f;
				letterSpacing = letterSize / 10;
				linkSize = letterSize * 0.75f;
				linkSpacing = linkSize / 10;

				textBoxExit = utility::menu::InitTextbox(letterSize * 2, 3.5f, 5, 9.1f);
			}

			void DrawCredits()
			{
				using namespace sokoban::config::visuals::menu;
				using namespace sokoban::utility::menu;

				//draw library credits
				DrawCredit(menuFont, "Library used: RayLib", letterSize, GRAY, 1);

				//draw graphics motor credits
				DrawCredit(menuFont, "Graphic Motor used: OpenGL", letterSize, GRAY, 2);

				//draw sound editor credits
				DrawCredit(menuFont, "Sound Recorder used: Audacity", letterSize, GRAY, 3);

				//draw start of music credits
				DrawCredit(menuFont, "Music used:", letterSize, GRAY, 4);

				//draw AcousticBreeze credits
				DrawCredit(menuFont, "'Acoustic Breeze', by Bensound", letterSize, GRAY, 4.5f);

				//draw AcousticBreeze download link
				DrawCredit(linkFont, "(downloaded in https://www.bensound.com/royalty-free-music/track/acoustic-breeze)", linkSize, GRAY, 5);

				//draw author credits
				DrawCredit(menuFont, "Made by Alejandro Insausti", letterSize, GRAY, 8);
			}

			void Draw()
			{
				using namespace sokoban::config::visuals::menu;

				BeginDrawing();

				//draw background
				DrawTexture(background, 0, 0, WHITE);

				//draw all credits
				DrawCredits();

				//draw game ver
				utility::menu::DrawGameVer(menuFont, letterSize, GRAY);
				//draw exit option
				utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize * 2, DARKGRAY, GRAY);

				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void CreditsMenu()
			{
				Init();

				do
				{
					UpdateMusicStream(music::menu);

					Draw();
				} while (!IsKeyReleased(config::keys::keys.quit) && !WindowShouldClose() &&
					(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));

				PlaySound(sounds::selectOption);
			}
		}
	}
}