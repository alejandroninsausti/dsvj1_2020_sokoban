#include "menu_settings.h"

#include "raylib.h"

#include "Utility/utility_menu.h"
#include "Utility/utility_keys.h"
#include "Config/visuals.h"
#include "Config/audio.h"
#include "Config/key_bindings.h"

using namespace sokoban::config::audio;
using namespace sokoban::config::visuals;
using namespace sokoban::config::visuals::menu;
using namespace sokoban::utility;

namespace sokoban
{
	namespace menu
	{
		namespace menu_settings
		{
			float letterSize;

			namespace settings_gameplay
			{
				using namespace sokoban::utility::menu;

				Rectangle textBoxUpKey;
				Rectangle textBoxDownKey;
				Rectangle textBoxLeftKey;
				Rectangle textBoxRightKey;

				Rectangle textBoxExit;

				void Init()
				{
					letterSize = screenWidth * 0.06f;

					textBoxUpKey = InitTextbox(letterSize, 7, 5, 3); //length, x, y
					textBoxDownKey = InitTextbox(letterSize, 9, 5, 7); //length, x, y
					textBoxLeftKey = InitTextbox(letterSize, 9, 3, 5); //length, x, y
					textBoxRightKey = InitTextbox(letterSize, 10, 7, 5); //length, x, y
					textBoxExit = InitTextbox(letterSize, 3.5f, 5, 9);
				}

				void DrawKeyBox(Rectangle textBox, int key)
				{
					Rectangle keyTextBox;
					float xTenthPosition = (textBox.x + textBox.width / 2) / screenWidth * 10;
					float yTenthPosition = textBox.y / screenHeight * 10;

					float letterSpacing = letterSize / 10;

					DrawRectangleRec(textBox, GRAY);
					if (key >= 39 && key <= 93)
					{
						DrawTextOnly(menuFont, TextFormat("%c", key), letterSize, DARKGRAY, xTenthPosition, yTenthPosition);
					}
					else
					{
						float longkeySize = letterSize * 0.675f;
						float longkeySpacing = longkeySize / 10;

						switch (key)
						{
						case KEY_SPACE:
							DrawTextOnly(menuFont, "SPACE", letterSize, DARKGRAY, xTenthPosition, yTenthPosition);
							break;
						case KEY_ESCAPE:
							DrawTextOnly(menuFont, "ESCAPE", letterSize, DARKGRAY, xTenthPosition, yTenthPosition);
							break;
						case KEY_ENTER://
							keyTextBox = utility::menu::InitTextbox(letterSize, 6, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "ENTER", keyTextBox, letterSize, letterSpacing, false, DARKGRAY);
							break;
						case KEY_TAB://
							keyTextBox = utility::menu::InitTextbox(letterSize, 4, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "TAB", keyTextBox, letterSize, letterSpacing, false, DARKGRAY);
							break;
						case KEY_BACKSPACE://
							keyTextBox = utility::menu::InitTextbox(letterSize, 10, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "BACKSPACE", keyTextBox, letterSize, letterSpacing, false, DARKGRAY);
							break;
						case KEY_RIGHT://
							keyTextBox = utility::menu::InitTextbox(letterSize, 1, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, ">", keyTextBox, letterSize, letterSpacing, false, DARKGRAY);
							break;
						case KEY_LEFT://
							keyTextBox = utility::menu::InitTextbox(letterSize, 1, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "<", keyTextBox, letterSize, letterSpacing, false, DARKGRAY);
							break;
						case KEY_DOWN://
							keyTextBox = utility::menu::InitTextbox(letterSize, 1, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "v", keyTextBox, letterSize, letterSpacing, false, DARKGRAY);
							break;
						case KEY_UP://
							keyTextBox = utility::menu::InitTextbox(letterSize, 1, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "^", keyTextBox, letterSize, letterSpacing, false, DARKGRAY);
							break;
						case KEY_LEFT_SHIFT:
							keyTextBox = utility::menu::InitTextbox(longkeySize, 13, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "LEFT SHIFT", keyTextBox, longkeySize, longkeySpacing, false, DARKGRAY);
							break;
						case KEY_RIGHT_SHIFT:
							keyTextBox = utility::menu::InitTextbox(longkeySize, 14, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "RIGHT SHIFT", keyTextBox, longkeySize, longkeySpacing, false, DARKGRAY);
							break;
						case KEY_LEFT_CONTROL:
							keyTextBox = utility::menu::InitTextbox(longkeySize, 15, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "LEFT CONTROL", keyTextBox, longkeySize, longkeySpacing, false, DARKGRAY);
							break;
						case KEY_RIGHT_CONTROL:
							keyTextBox = utility::menu::InitTextbox(longkeySize, 16, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "RIGHT CONTROL", keyTextBox, longkeySize, longkeySpacing, false, DARKGRAY);
							break;
						case KEY_LEFT_ALT:
							keyTextBox = utility::menu::InitTextbox(longkeySize, 11, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "LEFT ALT", keyTextBox, longkeySize, longkeySpacing, false, DARKGRAY);
							break;
						case KEY_RIGHT_ALT:
							keyTextBox = utility::menu::InitTextbox(longkeySize, 12, xTenthPosition, yTenthPosition);
							DrawTextRec(menuFont, "RIGHT ALT", keyTextBox, longkeySize, longkeySpacing, false, DARKGRAY);
							break;
						default:
							break;
						}
					}
				}

				void DrawOptions()
				{
					//draw dodge key option
					DrawKeyBox(textBoxUpKey, config::keys::keys.moveUp);
					DrawKeyBox(textBoxDownKey, config::keys::keys.moveDown);
					DrawKeyBox(textBoxLeftKey, config::keys::keys.moveLeft);
					DrawKeyBox(textBoxRightKey, config::keys::keys.moveRight);
					utility::menu::DrawTextboxTitle(menuFont, "Key Up", 7, textBoxUpKey, letterSize * 1.1f, GRAY);
					utility::menu::DrawTextboxTitle(menuFont, "Key Down", 9, textBoxDownKey, letterSize * 1.1f, GRAY);
					utility::menu::DrawTextboxTitle(menuFont, "Key Left", 9, textBoxLeftKey, letterSize * 1.1f, GRAY);
					utility::menu::DrawTextboxTitle(menuFont, "Key Right", 10, textBoxRightKey, letterSize * 1.1f, GRAY);
					utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize, DARKGRAY, GRAY);
				}

				void Draw()
				{
					BeginDrawing();

					//draw background
					DrawTexture(background, 0, 0, WHITE);

					//draw keys and exit options
					DrawOptions();
					ClearBackground(DARKGRAY);
					EndDrawing();
				}

				void GameplaySettings()
				{
					Init();
					int keyPressed = 0;

					do
					{
						UpdateMusicStream(music::menu);

						//Input | Update
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							if (utility::menu::OptionClicked(textBoxLeftKey))
							{
								PlaySound(sounds::selectOption);

								int newKey = utility::keys::InputKey();

								if (newKey > 0)
								{
									config::keys::keys.moveLeft = newKey;
								}
							}
						}

						//Draw
						Draw();
					} while (!IsKeyReleased(config::keys::keys.quit) && !WindowShouldClose() &&
						(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));

					PlaySound(sounds::selectOption);
				}
			}

			namespace settings_audio
			{
				Rectangle textBoxVolumeOnOff;

				Rectangle textBoxMusicOnOff;

				Rectangle textBoxSoundOnOff;

				Rectangle textBoxExit;;

				void Init()
				{
					letterSize = screenWidth * 0.06f;

					textBoxVolumeOnOff = utility::menu::InitTextbox(letterSize, 3, 5, 2);
					textBoxMusicOnOff = utility::menu::InitTextbox(letterSize, 3, 2.5f, 5);
					textBoxSoundOnOff = utility::menu::InitTextbox(letterSize, 3, 7.5f, 5);
					textBoxExit = utility::menu::InitTextbox(letterSize, 3.5f, 5, 9);
				}

				void DrawMuteOption(Rectangle textBox, float volume)
				{
					if (volume == 0)
					{
						utility::menu::DrawTextbox(menuFont, "OFF", textBox, letterSize, DARKPURPLE, RED);
					}
					else
					{
						utility::menu::DrawTextbox(menuFont, "ON", textBox, letterSize, DARKGREEN, GREEN);
					}
				}

				void DrawOptions()
				{
					//draw master volume option
					utility::menu::DrawTextboxTitle(menuFont, "Master Volume", 14, textBoxVolumeOnOff, letterSize, GRAY);
					DrawMuteOption(textBoxVolumeOnOff, masterVolume);

					//draw music volume option
					utility::menu::DrawTextboxTitle(menuFont, "Music Volume", 13, textBoxMusicOnOff, letterSize, GRAY);
					DrawMuteOption(textBoxMusicOnOff, musicVolume);

					//draw sound volume option
					utility::menu::DrawTextboxTitle(menuFont, "Sound Volume", 13, textBoxSoundOnOff, letterSize, GRAY);
					DrawMuteOption(textBoxSoundOnOff, soundVolume);

					//draw exit option
					utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize, DARKGRAY, GRAY);
				}

				void Draw()
				{
					BeginDrawing();

					//draw background
					DrawTexture(background, 0, 0, WHITE);

					//draw play, settings and exit options
					DrawOptions();
					ClearBackground(DARKGRAY);
					EndDrawing();
				}

				void AudioSettings()
				{
					Init();

					do
					{
						UpdateMusicStream(music::menu);

						//Input | Update
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							if (utility::menu::OptionClicked(textBoxVolumeOnOff))
							{
								PlaySound(sounds::selectOption);

								if (masterVolume == 1)
								{
									masterVolume = 0;
								}
								else
								{
									masterVolume = 1;
								}
								SetMasterVolume(0.5f * masterVolume);
							}
							else if (utility::menu::OptionClicked(textBoxSoundOnOff))
							{
								PlaySound(sounds::selectOption);

								if (soundVolume == 1)
								{
									soundVolume = 0;
								}
								else
								{
									soundVolume = 1;
								}

								sounds::Update();
							}
							else if (utility::menu::OptionClicked(textBoxMusicOnOff))
							{
								PlaySound(sounds::selectOption);

								if (musicVolume == 1)
								{
									musicVolume = 0;
								}
								else
								{
									musicVolume = 1;
								}

								music::Update();
							}
						}

						//Draw
						Draw();
					} while (!IsKeyReleased(config::keys::keys.quit) &&
						(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));

					PlaySound(sounds::selectOption);
				}
			}

			namespace settings_graphics
			{
				Rectangle textBoxActualResolution;

				Rectangle textBoxFullscreenOnOff;

				Rectangle textBoxExit;

				void Init()
				{
					letterSize = screenWidth * 0.06f;

					textBoxActualResolution = utility::menu::InitTextbox(letterSize, 8, 2.5f, 4);
					textBoxFullscreenOnOff = utility::menu::InitTextbox(letterSize, 3.5f, 7.5f, 4);
					textBoxExit = utility::menu::InitTextbox(letterSize, 3.5f, 5, 9);
				}

				void DrawActualResolution()
				{
					switch (res)
					{
					case RESOLUTIONS::LOW_RES:
						utility::menu::DrawTextbox(menuFont, "720x480", textBoxActualResolution, letterSize, ORANGE, GRAY);
						break;
					case RESOLUTIONS::MID_RES:
						utility::menu::DrawTextbox(menuFont, "800x600", textBoxActualResolution, letterSize, ORANGE, GRAY);
						break;
					case RESOLUTIONS::HIGH_RES:
						utility::menu::DrawTextbox(menuFont, "1152x900", textBoxActualResolution, letterSize, ORANGE, GRAY);
						break;
					default:
						break;
					}
				}

				void DrawFullscreenOnOffOption()
				{
					if (IsWindowFullscreen())
					{
						utility::menu::DrawTextbox(menuFont, "ON", textBoxFullscreenOnOff, letterSize, DARKGREEN, GREEN);
					}
					else
					{
						utility::menu::DrawTextbox(menuFont, "OFF", textBoxFullscreenOnOff, letterSize, DARKGREEN, GREEN);
					}
				}

				void DrawOptions()
				{
					//draw Resolution option
					utility::menu::DrawTextboxTitle(menuFont, "Resolution", 11, textBoxActualResolution, letterSize, GRAY);
					DrawActualResolution();

					//draw Fullscreen option
					utility::menu::DrawTextboxTitle(menuFont, "Fullscreen", 11, textBoxFullscreenOnOff, letterSize, GRAY);
					DrawFullscreenOnOffOption();

					//draw exit option
					utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize, DARKGRAY, GRAY);
				}

				void Draw()
				{
					BeginDrawing();

					//draw background
					DrawTexture(background, 0, 0, WHITE);

					//draw play, settings and exit options
					DrawOptions();
					ClearBackground(DARKGRAY);
					EndDrawing();
				}

				void GraphicsSettings()
				{
					Init();

					do
					{
						UpdateMusicStream(music::menu);

						//Input | Update
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							if (utility::menu::OptionClicked(textBoxActualResolution))
							{
								PlaySound(sounds::selectOption);

								switch (res)
								{
								case RESOLUTIONS::LOW_RES:
									res = RESOLUTIONS::MID_RES;
									InitScreenSize(res);
									UpdateBackgroundSize();
									break;
								case RESOLUTIONS::MID_RES:
									res = RESOLUTIONS::HIGH_RES;
									InitScreenSize(res);
									UpdateBackgroundSize();
									break;
								case RESOLUTIONS::HIGH_RES:
									res = RESOLUTIONS::LOW_RES;
									InitScreenSize(res);
									UpdateBackgroundSize();
									break;
								default:
									break;
								}
								Init();
							}
							else if (utility::menu::OptionClicked(textBoxFullscreenOnOff))
							{
								PlaySound(sounds::selectOption);

								ToggleFullscreen();
							}
						}

						//Draw
						Draw();
					} while (!IsKeyReleased(config::keys::keys.quit) &&
						(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));

					PlaySound(sounds::selectOption);
				}
			}

			Rectangle textBoxGameplay;

			Rectangle textBoxAudio;

			Rectangle textBoxGraphics;

			Rectangle textBoxExit;

			const float exitLetterModifier = 0.75f;

			void Init()
			{
				letterSize = screenWidth * 0.075f;

				textBoxGameplay = utility::menu::InitTextbox(letterSize, 8.25f, 5, 2);
				textBoxAudio = utility::menu::InitTextbox(letterSize, 4.5f, 5, 4);
				textBoxGraphics = utility::menu::InitTextbox(letterSize, 8, 5, 6);
				textBoxExit = utility::menu::InitTextbox(letterSize * exitLetterModifier, 3.5f, 5, 9);
			}

			void DrawOptions()
			{
				//draw gameplay option
				utility::menu::DrawTextbox(menuFont, "Gameplay", textBoxGameplay, letterSize, DARKGRAY, GRAY);

				//draw audio option
				utility::menu::DrawTextbox(menuFont, "Audio", textBoxAudio, letterSize, DARKGRAY, GRAY);

				//draw graphics option
				utility::menu::DrawTextbox(menuFont, "Graphics", textBoxGraphics, letterSize, DARKGRAY, GRAY);

				//draw exit option
				utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize * exitLetterModifier, DARKGRAY, GRAY);
			}

			void Draw()
			{
				BeginDrawing();

				//draw background
				DrawTexture(background, 0, 0, WHITE);

				//draw all the settings and the exit options
				DrawOptions();
				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void SettingsMenu()
			{
				Init();
				do
				{
					UpdateMusicStream(music::menu);

					//Input | Update
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
					{
						if (utility::menu::OptionClicked(textBoxGameplay))
						{
							PlaySound(sounds::selectOption);

							settings_gameplay::GameplaySettings();
							Init();
						}
						else if (utility::menu::OptionClicked(textBoxAudio))
						{
							PlaySound(sounds::selectOption);

							settings_audio::AudioSettings();
							Init();
						}
						else if (utility::menu::OptionClicked(textBoxGraphics))
						{
							PlaySound(sounds::selectOption);

							settings_graphics::GraphicsSettings();
							Init();
						}
					}

					//Draw
					Draw();
				} while (!IsKeyReleased(config::keys::keys.quit) &&
					(!IsMouseButtonPressed(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));

				PlaySound(sounds::selectOption);
			}
		}
	}
}


