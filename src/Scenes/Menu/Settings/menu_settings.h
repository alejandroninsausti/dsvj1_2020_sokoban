#ifndef MENU_SETTINGS_H
#define MENU_SETTINGS_H

#include "menu_settings.h"

namespace sokoban
{
	namespace menu
	{
		namespace menu_settings
		{
			void SettingsMenu();
		}
	}
}

#endif // !MENU_SETTINGS_H
