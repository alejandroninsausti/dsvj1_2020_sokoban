#include "menu.h"

#include "raylib.h"

#include "Utility/utility_menu.h"
#include "Utility/utility_calc.h"
#include "Config/audio.h"
#include "Config/key_bindings.h"
#include "Config/visuals.h"
#include "Scenes/Menu/Credits/menu_credits.h"
#include "Scenes/Menu/Settings/menu_settings.h"
#include "Scenes/Levels/levelManager.h"

using namespace sokoban::config::audio;
using namespace sokoban::config::visuals;
using namespace sokoban::utility;

namespace sokoban
{
	namespace menu
	{
		namespace menu_main
		{
			float letterSize;
			float titleSize;

			Rectangle textBoxPlay;

			Rectangle textBoxSettings;

			Rectangle textBoxCredits;

			Rectangle textBoxExit;

			void Init()
			{
				config::visuals::menu::Init();
				config::audio::InitMenu();

				letterSize = calc::NumToFloat(screenWidth) * 0.045f;
				titleSize = calc::NumToInt(letterSize * 4);

				textBoxPlay = utility::menu::InitTextbox(letterSize, 4, 5, 5);
				textBoxSettings = utility::menu::InitTextbox(letterSize, 8, 5, 6.25f);
				textBoxCredits = utility::menu::InitTextbox(letterSize, 7.25f, 5, 7.5f);
				textBoxExit = utility::menu::InitTextbox(letterSize, 3.5f, 5, 8.75f);

				PlayMusicStream(music::menu);
			}

			void DrawOptions()
			{
				using namespace sokoban::config::visuals::menu;

				//draw play option
				utility::menu::DrawTextbox(menuFont, "Play", textBoxPlay, letterSize, DARKGRAY, GRAY);

				//draw settings option
				utility::menu::DrawTextbox(menuFont, "Settings", textBoxSettings, letterSize, DARKGRAY, GRAY);

				//draw credits option
				utility::menu::DrawTextbox(menuFont, "Credits", textBoxCredits, letterSize, DARKGRAY, GRAY);

				//draw exit option
				utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize, DARKGRAY, GRAY);
			}

			void Draw()
			{
				using namespace sokoban::config::visuals::menu;
				using namespace sokoban::utility::menu;

				BeginDrawing();
				
				//draw background
				DrawTexture(background, 0, 0, WHITE);

				//draw title
				DrawTextOnly(menuFont, "Sokoban", titleSize, GRAY, 5, 0.5f);
				
				//draw play, settings and exit options
				DrawOptions();

				utility::menu::DrawGameVer(menuFont, letterSize * 0.75f, GRAY);

				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void MainMenu()
			{
				Init();

				do
				{
					UpdateMusicStream(music::menu);

					//Input | Update
					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
					{
						if (utility::menu::OptionClicked(textBoxPlay))
						{
							PlaySound(sounds::selectOption);
							levels::LevelManager();		
							Init();
						}
						else if (utility::menu::OptionClicked(textBoxSettings))
						{
							PlaySound(sounds::selectOption);
							menu_settings::SettingsMenu();
							Init();
						}
						else if (utility::menu::OptionClicked(textBoxCredits))
						{
							PlaySound(sounds::selectOption);
							menu_credits::CreditsMenu();
						}
					}

					//Draw
					Draw();
				} while (!IsKeyReleased(config::keys::keys.quit) && !WindowShouldClose() &&
					(!IsMouseButtonPressed(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));
			}
		}
	}
}