#include "game.h"

#include "raylib.h"

#include "Config/visuals.h"
#include "Config/audio.h"
#include "Config/key_bindings.h"
#include "Scenes/Menu/menu.h"

using namespace sokoban::config;

namespace sokoban
{
	void Init()
	{
		visuals::Init();
		audio::Init();
		keys::Init();
	}

	void DeInit()
	{
		CloseWindow(); //close Game
	}

	void StartGame()
	{
		Init();
		menu::menu_main::MainMenu();
		DeInit();
	}
}