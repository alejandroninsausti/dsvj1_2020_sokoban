#include "Config/visuals.h"

#include <iostream>
#include "utility_menu.h"
#include "utility_calc.h"

namespace sokoban
{
	namespace utility //new
	{
		namespace calc
		{
			bool IntIsWithinRange(int input, int min, int max)
			{
				return (input >= min && input <= max);
			}

			float GetAbsoluteValue(float number)
			{
				//if the number is positive, return number, else, multiply by -1 and return the positive version
				if (number > 0)
				{
					return number;
				}
				else
				{
					return number * -1;
				}
			}
			int GetAbsoluteValue(int number)
			{
				//if the number is positive, return number, else, multiply by -1 and return the positive version
				if (number > 0)
				{
					return number;
				}
				else
				{
					return number * -1;
				}
			}

			float GetSymbol(float number)
			{
				return GetAbsoluteValue(number) / number;
			}
			int GetSymbol(int number)
			{
				return GetAbsoluteValue(number) / number;
			}

			float NumToFloat(int i)
			{
				return static_cast<float>(i);
			}
			float NumToFloat(unsigned int ui)
			{
				return static_cast<float>(ui);
			}


			int NumToInt(float f)
			{
				return static_cast<int>(f);
			}

			short NumToShort(float f)
			{
				return static_cast<short>(f);
			}

			MatrixPos MoveInMatrix(MatrixPos pos, MOVEMENT move)
			{

				{
					short iMod = 0;
					short jMod = 0;

					switch (move)
					{
					case MOVEMENT::UP:
						iMod--;
						break;
					case MOVEMENT::DOWN:
						iMod++;
						break;
					case MOVEMENT::LEFT:
						jMod--;
						break;
					case MOVEMENT::RIGHT:
						jMod++;
						break;
					default:
						break;
					}

					pos.i += iMod;
					pos.j += jMod;

					return pos;
				}
			}
		}

		static bool IsALetter_Int(int input)
		{
			if (calc::IntIsWithinRange(input, (int)'a', (int)'z'))
			{
				return true;
			}
			else
			{
				return calc::IntIsWithinRange(input, (int)'A', (int)'Z');
			}
		}

		static int ChangeLetterCase_Int(int input, bool LowerToUpper)
		{
			short lowerUpperCaseDifference = (int)'a' - (int)'A';

			if (LowerToUpper)
			{
				if (input > (int)'Z')
				{
					input = input - lowerUpperCaseDifference;
				}
			}
			else
			{
				if (input < (int)'a')
				{
					input = input + lowerUpperCaseDifference;
				}
			}

			return input;
		}

		namespace keys
		{
			int InputKey()
			{
				int key = 0;

				key = GetKeyPressed();
				if (key != 0)
				{
					if (IsALetter_Int(key))
					{
						key = ChangeLetterCase_Int(key, true);
					}
					std::cout << key << std::endl;
					return key;
				}
				else if (IsKeyDown(KEY_ENTER))
				{
					return KEY_ENTER;
				}
				else if (IsKeyDown(KEY_TAB))
				{
					return KEY_TAB;
				}
				else if (IsKeyDown(KEY_BACKSPACE))
				{
					return KEY_BACKSPACE;
				}
				else if (IsKeyDown(KEY_RIGHT))
				{
					return KEY_RIGHT;
				}
				else if (IsKeyDown(KEY_LEFT))
				{
					return KEY_LEFT;
				}
				else if (IsKeyDown(KEY_DOWN))
				{
					return KEY_DOWN;
				}
				else if (IsKeyDown(KEY_UP))
				{
					return KEY_UP;
				}
				else if (IsKeyDown(KEY_LEFT_SHIFT))
				{
					return KEY_LEFT_SHIFT;
				}
				else if (IsKeyDown(KEY_RIGHT_SHIFT))
				{
					return KEY_RIGHT_SHIFT;
				}
				else if (IsKeyDown(KEY_LEFT_CONTROL))
				{
					return KEY_LEFT_CONTROL;
				}
				else if (IsKeyDown(KEY_RIGHT_CONTROL))
				{
					return KEY_RIGHT_CONTROL;
				}
				else if (IsKeyDown(KEY_LEFT_ALT))
				{
					return KEY_LEFT_ALT;
				}
				else if (IsKeyDown(KEY_RIGHT_ALT))
				{
					return KEY_RIGHT_ALT;
				}
				else
				{
					return -1;
				}
			}
		}

		namespace menu
		{
			bool OptionClicked(Rectangle textBox)
			{
				return CheckCollisionPointRec(GetMousePosition(), textBox);
			}

			Rectangle InitTextbox(float letterSize, float textLength, float xTenthPosition, float yTenthPosition)
			{
				Rectangle textbox;

				textbox.width = letterSize * (textLength / 2 + 0.5f); //width
				textbox.height = letterSize * 1.1f; //height

				//x position in a divided by 10 screen
				textbox.x = config::visuals::screenWidth / calc::NumToFloat(10) * xTenthPosition - textbox.width / 2;

				//y position in a divided by 10 screen
				textbox.y = config::visuals::screenHeight / calc::NumToFloat(10) * yTenthPosition;

				return textbox;
			}
			
			static Rectangle InitTextRec(float letterSize, float textLength, float xTenthPosition, float yTenthPosition)
			{
				Rectangle textbox;

				textbox.width = letterSize * (textLength / 2 + 0.5f); //width
				textbox.height = letterSize * 1.1f; //height

				//x position in a divided by 10 screen
				textbox.x = config::visuals::screenWidth / calc::NumToFloat(10) * xTenthPosition - textbox.width / 2;

				//y position in a divided by 10 screen
				textbox.y = config::visuals::screenHeight / calc::NumToFloat(10) * yTenthPosition;

				textbox.width *= 1.25f; //parent width with an extra

				return textbox;
			}

			static Rectangle InitTextTextbox(Rectangle textbox, float letterSize)
			{
				Rectangle textTextbox;

				textTextbox.width = textbox.width * 1.1f; //parent width with an extra
				textTextbox.height = textbox.height; //parent height
				textTextbox.x = textbox.x * 1.02f; //parent x position
				textTextbox.y = textbox.y; //parent y position

				return textTextbox;
			}

			void DrawCredit(Font font, const char* text, float letterSize, Color textColor, float yTenthPosition)
			{
				const float letterSpacing = letterSize / 10;

				Rectangle optionTextbox = InitTextbox(letterSize, TextLength(text) * 1.25f, 0, yTenthPosition);
				optionTextbox.x = 0.2f * (config::visuals::screenWidth / calc::NumToFloat(10));
				DrawTextRec(font, text, optionTextbox, letterSize, letterSpacing, false, textColor);
			}

			void DrawTextOnly(Font font, const char* text, float letterSize, Color textColor, float xTenthPosition, float yTenthPosition)
			{
				const float letterSpacing = letterSize / 10;

				Rectangle optionTextbox = InitTextRec(letterSize, calc::NumToFloat(TextLength(text)), xTenthPosition, yTenthPosition);
				DrawTextRec(font, text, optionTextbox, letterSize, letterSpacing, false, textColor);
			}

			void DrawTextbox(Font font, const char* text, Rectangle textbox, float letterSize, Color textColor, Color boxColor)
			{
				const float letterSpacing = letterSize / 10;

				DrawRectangleRec(textbox, boxColor);
				textbox = InitTextTextbox(textbox, letterSize);
				DrawTextRec(font, text, textbox, letterSize, letterSpacing, false, textColor);
			}

			void DrawGameVer(Font font, float letterSize, Color textColor)
			{
				const float letterSpacing = letterSize / 10;

				Rectangle gameVerTextbox = InitTextbox(letterSize, static_cast<float>(TextLength(gameVersion) * 1.25), 10, 10);
				gameVerTextbox.y -= gameVerTextbox.height;
				gameVerTextbox.x -= gameVerTextbox.width / 2;
				DrawTextRec(font, gameVersion, gameVerTextbox, letterSize, letterSpacing, false, GRAY);
			}

			void DrawTextboxTitle(Font font, const char* text, float textLength, Rectangle textbox, float letterSize, Color textColor)
			{
				const float letterSpacing = letterSize / 10;

				textbox.y -= config::visuals::screenHeight / calc::NumToFloat(10);
				textbox.x += textbox.width / 2; //reset X back to the width/10 position
				textbox.width = letterSize * (textLength / 2 + 0.5f); //set new width (because it's a new text)
				textbox.x -= textbox.width / 2; //apply the new width to x

				DrawTextRec(font, text, textbox, letterSize * 0.9f, letterSpacing, false, textColor);
			}
		}
	}
}