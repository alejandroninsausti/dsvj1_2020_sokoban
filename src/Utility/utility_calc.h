#ifndef UTILITY_CALC_H
#define UTILITY_CALC_H

namespace sokoban
{
	namespace utility //new
	{
		namespace calc
		{
			enum class MOVEMENT { NONE, UP, DOWN, LEFT, RIGHT };
			struct MatrixPos { short i; short j; };

			bool IntIsWithinRange(int input, int min, int max);

			float GetAbsoluteValue(float number);
			int GetAbsoluteValue(int number);

			float GetSymbol(float number);
			int GetSymbol(int number);

			float NumToFloat(int i);
			float NumToFloat(unsigned int ui);

			int NumToInt(float f);

			short NumToShort(float f);

			MatrixPos MoveInMatrix(MatrixPos pos, MOVEMENT move);
		}
	}
}

#endif // !UTILITY_CALC_H