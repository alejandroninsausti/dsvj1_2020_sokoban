#ifndef UTILITY_KEYS_H
#define UTILITY_KEYS_H

namespace sokoban
{
	namespace utility
	{
		namespace keys
		{
			int InputKey();
		}
	}
}

#endif // !UTILITY_KEYS_H