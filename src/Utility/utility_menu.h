#ifndef UTILITY_MENU_H
#define UTILITY_MENU_H

#include "raylib.h"

namespace sokoban
{
	namespace utility
	{
		namespace menu
		{
			const char gameVersion[] = "v1.2";

			bool OptionClicked(Rectangle textBox);
			Rectangle InitTextbox(float letterSize, float lengthOfString, float xTenthPosition, float yTenthPosition);
			Rectangle InitTextTextbox(Rectangle textbox, float letterSize);
			void DrawCredit(Font font, const char* text, float letterSize, Color textColor, float yTenthPosition);
			void DrawTextOnly(Font font, const char* text, float letterSize, Color textColor, float xTenthPosition, float yTenthPosition);
			void DrawTextbox(Font font, const char* text, Rectangle textbox, float letterSize, Color textColor, Color boxColor);
			void DrawGameVer(Font font, float letterSize, Color textColor);
			void DrawTextboxTitle(Font font, const char* text, float textLength, Rectangle textbox, float letterSize, Color textColor);
		}
	}
}

#endif // !UTILITY_MENU_H